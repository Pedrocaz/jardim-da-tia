<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('client', 'ClientController')->except([
    'edit', 'update'
]);

Route::resource('game', 'QuestionController')->except([
    'create', 'edit', 'destroy'
]);
Route::post('game/reset', 'QuestionController@reset');

Route::resource('newsletter', 'NewsletterController')->only([
    'index', 'create', 'store'
]);

Route::resource('config', 'CardController')->only([
    'edit', 'update'
]);

Route::resource('sale', 'SaleController')->only([
    'create','store','edit','update'
]);
Route::put('sale/{id}/{item}/remove','SaleController@removeItem');
Route::put('sale/{id}/{item}/update','SaleController@updateItem');
Route::put('sale/{id}/pay','SaleController@pay');
Route::delete('sale/{id}/cancel','SaleController@cancel');

Route::put('sale/{id}/{item}/{extra}/removeExtra','SaleController@removeExtra');
Route::put('sale/{id}/{item}/{extra}/updateExtra','SaleController@updateExtra');
Route::put('sale/{id}/{item}/addExtra','SaleController@addExtra');