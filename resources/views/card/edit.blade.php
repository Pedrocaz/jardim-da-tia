@extends('master')

@section('messages')
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection

@section('content')

    <div class="container">
        <form action="{{route('config.update',1)}}" method="post">
            @csrf
            @method('PUT')
            <div class="card col-5 mx-auto border-2 border-success p-0">
                <div class="card-header bg-success text-white">
                    <h3>Card Config</h3>
                </div>
                <div class="card-body">
                    <div class="mb-4 form-inline justify-content-center">
                        <label for="money">Money Threshold:&nbsp;</label>
                        <input type="number" name="money" class="form-control col-3" value="{{$options[0]}}" required/>
                        <label for="money">&nbsp;€</label>
                    </div>
                    <hr class="bg-success">
                    <div class="mb-4 form-inline justify-content-center">
                        <label for="points">Points received when threshold is met:&nbsp;</label>
                        <input type="number" name="points" class="form-control col-3" value="{{$options[1]}}" required/>
                    </div>
                    <hr class="bg-success">
                    <div class="form-inline justify-content-center">
                        <label for="ratio">Ratio of&nbsp;</label>
                        <input type="number" name="ratio" class="form-control col-3" value="{{$options[2]}}" required/>
                        <label for="ratio">&nbsp;points per 1€</label>
                    </div>
                </div>

                <div class="card-footer bg-success text-center">
                    <button type="submit" class="btn btn-dark">Update</button>
                </div>
            </div>
        </form>
    </div>
@endsection