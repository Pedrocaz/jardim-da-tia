@extends('master')

@section('content')

    <div class="mx-auto jumbotron col-md-10 m-0 pb-3">
        <div class="row justify-content-center">
            <h3>{{$question->question}}</h3>
        </div>
        <form action="{{route('game.update',$nif)}}" method="post">
            @method('PUT')
            @csrf

            <div class="form-row justify-content-around mt-3 p-0">
                <div class="btn-group btn-group-toggle col-lg-10 m-1 p-0" data-toggle="buttons">
                    @foreach($question->answers as $answer)
                        <label class="btn btn-outline-primary btn-lg form-check-label col-lg-10 m-1 ">
                            <input class="form-check-input" type="radio" name="answer"
                                   value="{{$answer->id}}"/>{{$answer->answer}}
                        </label>
                    @endforeach
                </div>
            </div>
            <input type="hidden" name="question_id" value="{{$question->id}}">
            <div class="col-lg-10 p-0 mx-auto">
                <div class="form-row my-3 p-0  justify-content-center">
                    <input type="submit" class="btn btn-success" value="Answer"/>
                </div>
            </div>
        </form>
    </div>
@endsection
