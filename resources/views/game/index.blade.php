@extends('master')

@section('messages')
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($type=Session::get('type'))
        <div class="alert alert-{{$type}}">
            <p>{!!Session::get('message')!!}</p>
        </div>
    @endif
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <form action="{{route('game.store')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-success text-white">NIF</span>
                    </div>
                    <input type="number" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="nif"
                           required>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit" id="button-addon2">Play</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row justify-content-center mt-3">
            <form action="game/reset" method="post">
                @csrf
                <input type="submit" class="btn btn-lg btn-warning" value="Reset played status"/>
            </form>
        </div>
    </div>
@endsection