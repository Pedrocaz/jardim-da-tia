<table class="table table-striped table-hover table-sm tab border-success">
    <thead class="bg-success text-white">
    <tr>
        <th colspan="5" class="text-center">Products</th>
    </tr>
    <tr>
        <th>Image</th>
        <th>Code</th>
        <th>Description</th>
        <th>Category</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td class="align-middle"><img src="{!!$product->image!!}" height="50px"/></td>
            <td class="align-middle">{{ $product->code }}</td>
            <td class="align-middle">{{ $product->description }}</td>
            <td class="align-middle">{!! Str::after($product->type,'Uac\SaleSys\Business\\') !!}</td>
            <td class="align-middle"><span class="text-{{ $product->price > $product->getPrice() ? "danger" : "dark" }}">{{$product->getPrice()}}€</span></td>
        </tr>
    @endforeach
    </tbody>
</table>

