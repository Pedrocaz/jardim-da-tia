@extends('master')

@section('messages')
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>New Client</h3>
            </div>
        </div>


        <form action="{{route('client.store')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <label for="name">Name:</label>
                    <input type="text" name="name" class="form-control" placeholder="Please write your name.."
                           required/>
                </div>
                <div class="col-md-12">
                    <label for="email">Email:</label>
                    <input type="email" name="email" class="form-control" placeholder="Please write your email.."
                           required/>
                </div>
                <div class="col-md-12">
                    <label for="nif">NIF:</label>
                    <input type="number" name="nif" class="form-control" placeholder="Please write your nif.."
                           required/>
                </div>
                <div class="col-md-12">
                    <label for="withCard">Card:</label>
                    <input type="radio" name="withCard" value="true" checked> Yes &nbsp
                    <input type="radio" name="withCard" value="false"> No
                </div>
                <div class="col-md-12">
                    <a href="{{route('client.index')}}" class="btn btn-sm btn-success">Back</a>
                    <button type="submit" class="btn btn-sm btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
@endsection