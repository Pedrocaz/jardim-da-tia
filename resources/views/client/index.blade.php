@extends('master')

@section('messages')
    @if($message=Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
    @endif
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Client List</h3>
            </div>
            <div class="col-sm-2">
                <a class="btn btn-sm btn-success" href="{{route('client.create')}}">Create new Client</a>
            </div>
        </div>

        <table class="table table-hover table-sm">
            <thead class="bg-success text-white">
            <tr>
                <th>Nº</th>
                <th>Name</th>
                <th>Email</th>
                <th>NIF</th>
                <th>Card</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->email }}</td>
                    <td>{{ $client->nif }}</td>
                    <td>{{ $client->card ? "Yes" : "No" }}</td>
                    <td>
                        <form action="{{route('client.destroy',$client->nif)}}" method="post">
                            <a class="btn btn-sm btn-info" href="{{route('client.show',$client->nif)}}">Details</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $clients->links() !!}
    </div>
@endsection