@extends('master')

@section('messages')
    @if($message=Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
    @endif
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$client->name}}'s Details</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3  mx-auto ">
                <div class="card p-0 mt-5 border-success">
                    <div class="card-header bg-success text-center text-white">
                        <h5 class="card-title m-0">Personal Info</h5>
                    </div>
                    <div class="card-body p-0">
                        <div class="form-group row m-0 p-0">
                            <div class="col-4 text-center m-0 p-0 border-right border-success p-2"><strong>Name</strong>
                            </div>
                            <div class="col-8 p-0 m-0 p-2">{{$client->name}}</div>
                        </div>
                        <hr class="bg-success m-0">
                        <div class="form-group row m-0 p-0">
                            <div class="col-4 text-center m-0 p-0 border-right border-success p-2">
                                <strong>Email</strong>
                            </div>
                            <div class="col-8 p-0 m-0 p-2">{{$client->email}}</div>
                        </div>
                        <hr class="bg-success m-0">
                        <div class="form-group row m-0 p-0">
                            <div class="col-4 text-center m-0 p-0 border-right border-success p-2"><strong>NIF</strong>
                            </div>
                            <div class="col-8 p-0 m-0 p-2">{{$client->nif}}</div>
                        </div>

                    </div>
                </div>
            </div>
            @if($client->card)
                <div class="col-xl-2 mx-auto">
                    <div class="card p-0 mt-5  border-success">
                        <div class="card-header bg-success text-center text-white">
                            <h5 class="card-title m-0">Card</h5>
                        </div>
                        <div class="card-body p-0">
                            <div class="form-group row m-0 p-0">
                                <div class="col-8 text-center m-0 p-0 border-right border-success p-2">
                                    <strong>Points</strong>
                                </div>
                                <div class="col-4 p-0 m-0 p-2">{{$client->card->points}}</div>
                            </div>
                            <hr class="bg-success m-0">
                            <div class="form-group row m-0 p-0">
                                <div class="col-8 text-center m-0 p-0 border-right border-success p-2">
                                    <strong>Threshold</strong></div>
                                <div class="col-4 p-0 m-0 p-2">{{$client->card->threshold}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-xl-4  mx-auto ">
                <div class="card p-0 mt-5 border-success">
                    <div class="card-header bg-success text-center text-white">
                        <h5 class="card-title m-0">Sales History</h5>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped table-sm border-success col-12 m-0 text-center">
                            <thead class="bg-success text-white">
                            <tr>
                                <th>#</th>
                                <th>Total</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sales as $sale)
                                <tr>
                                    <td class="align-middle">{{ $loop->iteration }}</td>
                                    <td class="align-middle">{{ $sale->total }} €</td>
                                    <td class="align-middle">{{ $sale->updated_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <a href="{{route('client.index')}}" class="btn btn-sm btn-success">Back</a>
        </div>
    </div>


@endsection