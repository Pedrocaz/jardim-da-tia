@extends('master')

@section('content')

    <div class="container">
        <div class="row justify-content-around">
            <a href="/" class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button">
                <i class="fas fa-4x fa-th py-2"></i>
                <h2 class="h5 p-1">Dashboard</h2>
            </a>

            <a class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button"
               href="/newsletter">
                <i class="far fa-4x fa-newspaper"></i>
                <h2 class="h5 p-1">Newsletter</h2>
            </a>
            <a class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button"
               href="{{route('sale.create')}}">
                <i class="fas fa-4x fa-shopping-cart m-1"></i>
                <h2 class="h5 p-1">Sale</h2>
            </a>
            <a class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button"
               href="/client">
                <i class="fas fa-4x fa-users m-1"></i>
                <h2 class="h5 p-1">Clients</h2>
            </a>
            <a class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button"
               href="/game">
                <i class="fas fa-4x fa-gamepad m-1"></i>
                <h2 class="h5 p-1">Game</h2>
            </a>
            <a class="btn btn-success btn-lg text-white col-lg-3 col-md-4 col-sm-8 mb-5 mr-3" role="button"
               href="/config/1/edit">
                <i class="fas fa-4x fa-cog m-1"></i>
                <h2 class="h5 p-1">Config</h2>
            </a>


        </div>
    </div>

@endsection