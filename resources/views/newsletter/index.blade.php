@extends('master')

@section('messages')
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($type=Session::get('type'))
        <div class="alert alert-{{$type}}">
            <p>{!!Session::get('message')!!}</p>
        </div>
    @endif
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-2">
                <a class="btn btn-sm btn-success" href="{{route('newsletter.create')}}">Generate New Newsletter</a>
            </div>
        </div>
        <div class="row justify-content-around">
            @foreach($news as $new)
                <div class="card text-center p-0 mt-5 mx-auto col-5 border-success">
                    <div class="card-header bg-success text-white">
                        <h5 class="card-title">Newsletter #{{$new->id}} - {{$new->suggestions[0]->season->name}}</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($new->suggestions as $suggestion)
                                <li class="list-group-item border-success">{{$suggestion->text}}</li>
                            @endforeach
                        </ul>

                        <hr class="bg-success">

                        @if(!$new->promotions->isEmpty())
                            <div class="bd-example">
                                <div id="newsletter{{$loop->iteration}}" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        @foreach($new->promotions as $promotion)
                                            <li data-target="#newsletter" data-slide-to="{{$loop->iteration}}"
                                                class="bg-success @if ($loop->first) active @endif"></li>
                                        @endforeach
                                    </ol>
                                    <div class="carousel-inner">
                                        @foreach($new->promotions as $promotion)
                                            <div class="carousel-item @if ($loop->first) active @endif">
                                                <img src="{{$promotion->products[0]->image}}"
                                                     class="d-block w-100">
                                                <div class="carousel-caption rounded text-danger"
                                                     style="background-color: rgba(128,128,128,0.25)">
                                                    <p class="h5">{{$promotion->description}}</p>
                                                    <p class="h6">{{$promotion->products[0]->description}}
                                                        with <strong>{{$promotion->percentage*100}}%</strong> Discount
                                                    </p>
                                                    <p class="h6">Before: {{$promotion->products[0]->price}}€
                                                        <strong>Now: {{number_format($promotion->products[0]->price*(1-$promotion->percentage),2)}}
                                                            €</strong>
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev text-success"
                                       href="#newsletter{{$loop->iteration}}"
                                       role="button"
                                       data-slide="prev">
                                        <span><i class="fa fa-2x fa-angle-left " aria-hidden="true"></i></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next text-success"
                                       href="#newsletter{{$loop->iteration}}"
                                       role="button"
                                       data-slide="next">
                                        <span><i class="fa fa-2x fa-angle-right " aria-hidden="true"></i></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        @else
                            <ul class="list-group">
                                <li class="list-group-item border-success">There are currently no promotions at this
                                    season.
                                </li>
                            </ul>
                        @endif
                    </div>

                    <div class="card-footer bg-success text-white">
                        Created at {{ date('M d, o', strtotime($new->publicationDate))}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection