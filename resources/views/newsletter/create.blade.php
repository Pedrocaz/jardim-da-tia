@extends('master')

@section('messages')
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection

@section('content')

    <div class="jumbotron col-5 mx-auto border border-success py-4">
        <h3 class="text-center">Generate Newsletter</h3>
        <hr>
        <form class="mb-0" action="{{route('newsletter.store')}}" method="post">
            @csrf
            <div class="form-inline justify-content-around">
                <label for="season">Generate the newsletter for the&nbsp;<strong>season</strong>:</label>
                <select id="season" class="custom-select" name="season">
                    @foreach($seasons as $season)
                        <option value="{{$season->id}}">{{$season->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="row justify-content-between">
                <a href="{{route('newsletter.index')}}" class="btn btn-sm btn-success m-3">Back</a>
                <button type="submit" class="btn btn-sm btn-primary m-3">Generate</button>
            </div>

        </form>
    </div>
@endsection