@extends('master')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="card text-center p-0 mt-5 mx-auto col-6 border-success">
                <div class="card-header bg-success text-white">
                    <h5 class="card-title">Jardim da Tia</h5>
                </div>
                <div class="card-body p-0">
                    <table class="table table-sm border-success col-12 m-0">
                        <thead class="bg-success text-white">
                        <tr>
                            <th class="text-center" colspan="7">{{$sale->client->name}}'s Sale Items</th>
                        </tr>
                        <tr>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Sub-total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sale->items as $item)
                            <tr>
                                <td class="align-middle">{{ $item->product->code }}</td>
                                <td class="align-middle">{{ $item->product->description }}</td>
                                <td class="align-middle">{{ $item->product->price }}€</td>
                                <td class="align-middle">{{ $item->quantity }}</td>
                                <td class="align-middle">{{ $item->quantity*$item->product->getPrice() }}€</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr class="bg-success">
                    @if($pointsUsed)
                        <p class="h5">You used {{$pointsUsed}} points.</p>
                    @endif
                    <p class="h5">Total: {{$newTotal}}€</p>
                    <p class="h5">Paid: {{$payment}}€</p>
                    <p class="h5">Change: {{$change}}€</p>
                </div>
                <div class="card-footer bg-success text-white">
                    Processed at {{date('Y-m-d H:i:s')}}
                </div>
            </div>

        </div>
        <div class="row justify-content-center">
            <a href="{{route('sale.create')}}" class="btn btn-sm btn-success m-3">Continue</a>
        </div>
    </div>
@endsection