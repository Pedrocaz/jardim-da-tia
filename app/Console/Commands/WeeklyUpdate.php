<?php

namespace Uac\SaleSys\Console\Commands;

use Illuminate\Console\Command;
use Uac\SaleSys\Business\SaleSysFacade;

class WeeklyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'week:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset played to all clients.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SaleSysFacade $saleSysFacade)
    {
        $gameHandler = $saleSysFacade->getGameHandler();
        $gameHandler->resetPlayers();
        $this->info("Weekly played has been reset.");
    }
}
