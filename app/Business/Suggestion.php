<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    //
    /**
     * Use to define one-to-one relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * Gives all suggestion from the season given
     *
     * @param int $season_id
     * @return mixed
     */
    public static function findBySeason(int $season_id)
    {
        return Suggestion::where('season_id', $season_id)->get();
    }
}
