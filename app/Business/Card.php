<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{

    /**
     * Add money to the card threshold
     *
     * @param double $money adds money to threshold of the card, and in case it surpassasses it, gives the points
     */
    public function addMoneyToThreshold(float $money): void
    {
        /*I did a for incrementing one at the time because the money could be many times bigger than the threshold,
         and i wouldn't know how many times the bonus would apply*/
        for ($i = 0; $i < round($money); $i++) {
            $this->increment('threshold');
            if ($this->threshold > ConfigHelper::getMoneyThreshold()) {
                $this->threshold = 0;
                $this->points += ConfigHelper::getPointsReceived();
                $this->save();//threshold was not being reset to 0, this fixed
            }
        }
        $this->save();
    }
}
