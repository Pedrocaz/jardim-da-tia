<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 22/06/2019
 * Time: 02:37
 */

namespace Uac\SaleSys\Business;


use Illuminate\Database\Eloquent\Model;

final class ConfigHelper extends Model
{
    protected $table = 'config';

    /**
     * Gives the value of the variable money_threshold
     *
     * @return int with the value of money that will give points.
     */
    public static function getMoneyThreshold(): int
    {
        return self::all()->first()->money_threshold;
    }

    /**
     * Gives the value of the variable points_received
     *
     * @return int with the amount of points to be received when money threshold is met.
     */
    public static function getPointsReceived(): int
    {
        return self::all()->first()->points_received;
    }

    /**
     * Gives the value of the variable points_per__euro
     *
     * @return int that represents the ratio of points to euro.
     */
    public static function getPointsPerEuro(): int
    {
        return self::all()->first()->points_per_euro;
    }

    /**
     * Sets the value of the variable money_threshold
     *
     * @param int $money set the new threshold
     */
    public static function setMoneyThreshold(int $money): void
    {
        $m = self::all()->first();
        $m->money_threshold = $money;
        $m->save();
    }

    /**
     * Sets the value of the variable points_received
     *
     * @param int $points set the new points
     */
    public static function setPointsReceived(int $points): void
    {
        $m = self::all()->first();
        $m->points_received = $points;
        $m->save();
    }

    /**
     * Sets the value of the variable points_per_euro
     *
     * @param int $points set the new value for points to be received
     */
    public static function setPointsPerEuro(int $points): void
    {
        $m = self::all()->first();
        $m->points_per_euro = $points;
        $m->save();
    }

}