<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * Use to define one-to-one relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rightAnswer()
    {
        return $this->belongsTo(Answer::class);
    }

    /**
     * Use to define one-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * Return true if the id given is the right answer or false if not
     *
     * @param int $id of the answer to be checked
     * @return bool return true in case the answer is right or false if otherwise
     */
    public function checkAnswer(int $id):bool
    {
        return $id==$this->rightAnswer->id;
    }

}
