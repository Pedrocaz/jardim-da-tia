<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;
use Uac\SaleSys\Exceptions\ApplicationException;

class Client extends Model
{
    //
    /**
     * Create a new client with the parameters given
     *
     * @param string $name client name
     * @param string $email client email
     * @param int $nif client nif
     * @param bool|null $withCard if the client wants card or not, true for positive false for negative
     * @return Client client instance after created
     * @throws ApplicationException it occurs when teh nif given does not have 9 digits or the client nif is already registered
     */
    public static function createWith(string $name, string $email, int $nif, ?bool $withCard): Client
    {

        if (!static::validNIF($nif)) {
            throw new ApplicationException("NIF needs to have 9 digits.");
        }

        if (static::findByNIF($nif) != null) {
            throw new ApplicationException("This client already exists.");
        }

        $client = new Client();

        $client->name = $name;
        $client->email = $email;
        $client->nif = $nif;
        $client->played = false;
        if ($withCard) {
            $card = new Card();
            $card->points = 0;
            $card->threshold = 0;
            $card->save();
            $client->card()->associate($card);
        }

        $client->save();

        return $client;
    }

    /**
     * returns the client object with the nif given
     *
     * @param int $nif client nif
     * @return Client|null returns the client object or null in case it doesnt exists
     */
    public static function findByNIF(int $nif): ?Client
    {
        return Client::where('nif', $nif)->first();
    }

    /**
     * deletes the client object with the nif given
     *
     * @param int $nif client nif
     */
    public static function deleteByNIF(int $nif): void
    {
        Client::where('nif', $nif)->delete();
    }

    /**
     * Checks if the nif given has 9 digits
     *
     * @param int $nif client nif
     * @return bool true if it has 9 digits or false if not
     */
    public static function validNIF(int $nif): bool
    {
        if (strlen((string)$nif) == 9) {
            return true;
        }
        return false;
    }

    /**
     * Use to define one-to-one relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    /**
     * Use to define one-to-Many relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    /**
     * Use to define many-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questionsHistory()
    {
        return $this->belongsToMany(Question::class)->as('history');
    }

    /**
     *Resets the played status on all clients
     */
    public function resetPlay(): void
    {
        $this->played = false;
        $this->save();
    }


}
