<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 26/06/2019
 * Time: 17:32
 */

namespace Uac\SaleSys\Business;


class ConfigHandler
{
    /**
     * Gives the config options
     *
     * @return array in the data in the following order (MoneyThreshold, PointsReceived, PointsPerEuro)
     */
    public function getOptions(): array
    {
        $money = ConfigHelper::getMoneyThreshold();
        $points = ConfigHelper::getPointsReceived();
        $ratio = ConfigHelper::getPointsPerEuro();
        return array($money, $points, $ratio);
    }

    /**
     * Set the config options
     *
     * @param double $money of MoneyThreshold
     * @param int $points of PointsReceived
     * @param int $ratio of PointsPerEuro
     */
    public function setOptions(int $money, int $points, int $ratio): void
    {
        ConfigHelper::setMoneyThreshold($money);
        ConfigHelper::setPointsReceived($points);
        ConfigHelper::setPointsPerEuro($ratio);
    }

}