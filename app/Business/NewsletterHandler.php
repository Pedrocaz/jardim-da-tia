<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 21/06/2019
 * Time: 15:06
 */

namespace Uac\SaleSys\Business;

class NewsletterHandler
{
    /**
     * Calls the function CreateWith from Newsletter to create a new newsletter
     *
     * @param int $season_id
     * @return Newsletter
     */
    public function createNewsletterForSeason(int $season_id): Newsletter
    {
        return Newsletter::createWith($season_id);
    }

    /**
     * return all newsletters
     *
     * @return \Illuminate\Database\Eloquent\Collection|Newsletter[]
     */
    public function getNewsletters()
    {
        return Newsletter::all();
    }
}