<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 16/06/2019
 * Time: 12:04
 */

namespace Uac\SaleSys\Business;

use Uac\SaleSys\Exceptions\ApplicationException;

class GameHandler
{
    /**
     * Check if the client with the nif given has already played this week
     *
     * @param int $nif client nif
     * @return bool true if has played or false if not
     */
    public function clientPlayed(int $nif): bool
    {
        return Client::findByNIF($nif)->played;
    }

    /**
     * Gives a random question from data base to the client answer
     *
     * @param int $nif client nif
     * @return Question to the client answer
     * @throws ApplicationException if there are no questions unanswered
     */
    public function getRandomQuestion(int $nif): Question
    {
        try {
            return GameSingleton::getInstance()->getRandomQuestionOfThisSeason($nif);
        } catch (ApplicationException $e) {
            throw  $e;
        }
    }

    /**
     * Checks if the answer selected by client is correct or incorrect
     *
     * @param int $nif client nif
     * @param int $question_id question id that was given to be answered
     * @param int $answer_id id selected by client
     * @return bool true if correct or false if not
     */
    public function checkAnswer(int $nif, int $question_id, int $answer_id): bool
    {
        $client = Client::findByNIF($nif);
        $client->played = true;

        //Add question to client history
        $client->questionsHistory()->attach($question_id);

        if (Question::find($question_id)->checkAnswer($answer_id)) {
            //Right Answer
            $client->card->increment('points');
            $client->save();
            return true;
        } else {
            //Wrong Answer
            $client->save();
            return false;
        }
    }

    /**
     * Resets clients played status
     */
    public function resetPlayers(): void
    {
        foreach (Client::get() as $client) {
            $client->resetPlay();
        }
    }
}