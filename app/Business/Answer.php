<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    /**
     *  Use to define one-to-one relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
