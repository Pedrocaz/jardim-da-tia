<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{

    /**
     * Use to define many-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Returns all promotion in season given
     *
     * @param int $season_id of the season that you want all promotions from.
     * @return \Illuminate\Database\Eloquent\Collection|Promotion[] Collection of promotions of the season you want
     */
    public static function findBySeason(int $season_id)
    {
        $promotions = Promotion::all()->filter(function ($promotion) use ($season_id) {
            //The middle date of the promotion will determine what season it belongs to.
            $middleDate = (($promotion->startDate) + ($promotion->endDate))/2;
            $promotion_season_id = Season::timestampToSeason(\DateTime::createFromFormat("U.u",$middleDate/1000))->id;
            return $promotion_season_id == $season_id;
        });
        return $promotions;
    }

}
