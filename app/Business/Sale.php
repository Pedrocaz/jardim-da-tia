<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;
use Uac\SaleSys\Exceptions\ApplicationException;

/**
 * @property mixed items
 */
class Sale extends Model
{

    /**
     * Use to define one-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * Use to define one-to-one relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Adds a new Item to the sale
     *
     * @param Integer $code product code to be add
     * @param Integer $quantity the quantity to be add
     * @throws ApplicationException occurs when there is no product with the given code
     */
    public function newItem(int $code, int $quantity): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            $i = $this->items->where('product_id', $prod->id)->first();
            if ($i) {
                $i->quantity += $quantity;
                $i->save();
            } else {
                $this->items()->save(Sale::addItem($quantity, $prod));
            }
            $this->calculateTotal();
        }
    }

    /**
     * Updates and item on the sale
     *
     * @param Integer $code product code to be updated
     * @param Integer $quantity the quantity to be updated
     * @throws ApplicationException occurs when there is no product with the given code or the product with that code does not exist in the sale
     */
    public function updateItem(int $code, int $quantity): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        }
        elseif($quantity<=0){
            $this->removeItem($code);
            $this->calculateTotal();
        }
        else {
            $i = $this->items->where('product_id', $prod->id)->first();
            if ($i) {
                $i->quantity = $quantity;
                $i->save();
                $this->calculateTotal();
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this sale.");
            }
        }
    }

    /**
     * Removes an item from the sale
     *
     * @param Integer $code product code to be withdrawn
     * @throws ApplicationException occurs when there is no product with the given code or the product with that code does not exist in the sale
     */
    public function removeItem(int $code): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            $i = $this->items->where('product_id', $prod->id)->first();
            if ($i) {
                $i->delete();
                $this->save();
                $this->calculateTotal();
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this sale.");
            }
        }
    }


    /**
     * Creates a new Item instance
     *
     * @param Integer $quantity amount of the product on the item
     * @param Product $prod product used to make the associate function
     * @return Item the new item
     */
    public static function addItem(int $quantity, Product $prod): Item
    {
        $item = new Item();
        $item->quantity = $quantity;
        $item->product()->associate($prod);
        return $item;
    }

    /**
     * Creates a new Sale instance
     *
     * @param Client $client client used to make the associate function
     * @return Sale the new sale
     */
    public static function newSale(Client $client)
    {
        $sale = new Sale();

        $sale->total = 0;
        $sale->client()->associate($client);
        $sale->save();

        return $sale;
    }

    /**
     * Add a product to the bouquet
     *
     * @param Bouquet $bouquet bouquet to add product
     * @param int $code product code to add to bouquet
     * @param int $quantity product quantity to add to bouquet
     * @throws ApplicationException occurs when there is no product with the given code or the bouquet does not exist in this sale
     */
    public function addProductBouquet(Bouquet $bouquet, int $code, int $quantity)
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            if ($bouquet) {
                $bouquet->addProduct($code, $quantity);
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this sale.");
            }
        }
    }

    /**
     * Updates the product quantity in the bouquet
     *
     * @param Bouquet $bouquet bouquet to update product
     * @param int $code product code to update to bouquet
     * @param int $quantity product quantity to be updated
     * @throws ApplicationException occurs when there is no product with the given code or the bouquet does not exist in this sale
     */
    public function updateProductBouquet(Bouquet $bouquet, int $code, int $quantity)
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            if ($bouquet) {
                $bouquet->updateProduct($code, $quantity)->save();
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this sale.");
            }
        }
    }

    /**
     * Removes a product from bouquet
     *
     * @param Bouquet $bouquet bouquet to remove products
     * @param int $code product code to remove from bouquet
     * @param int $quantity product quantity to remove from bouquet
     * @throws ApplicationException occurs when there is no product with the given code or the bouquet does not exist in this sale
     */
    public function removeProductBouquet(Bouquet $bouquet, int $code, int $quantity)
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            if ($bouquet) {
                $bouquet->removeProduct($code, $quantity);
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this sale.");
            }
        }
    }

    /**
     * Gives all Items on the sale
     *
     * @return mixed list of sale items
     */
    public function showItems()
    {
        return $this->items->all();
    }

    /**
     * Calculates the item total price
     *
     * @return float total of the sale
     */
    public function calculateTotal(): float
    {
        $this->total = 0;
        foreach ($this->items as $item) {
            $this->total += $item->quantity * $item->product->getPrice();
        }
        $this->save();
        return $this->total;
    }

    /**
     * Finishes the sale and closes it
     *
     * @param float $payment given by client
     * @param float $new_total total with possible discount consideration
     * @param int points used by client
     * @return float change to give to the client
     * @throws ApplicationException if there is no items in the sale or the amount given is lower than the total
     */
    public function pay(float $payment, float $new_total, ?int $points): float{
        if($this->total==0){
            throw new ApplicationException("No items registered.");
        }
        $change = $payment - $new_total;
        if($change<0){
            throw new ApplicationException("The amount given is not enough to pay for the sale.");
        }
        $this->isOpen = false;
        if($this->client->card){
            $this->client->card->points -= $points;
            $this->client->card->addMoneyToThreshold($new_total);
            $this->client->card->save();
        }
        $this->total = $new_total;
        $this->save();
        return $change;
    }

    /**
     * Deletes the actual sale if cancel button is pressed
     *
     * @throws \Exception
     */
    public function deleteThis():void {
        $this->delete();
    }
}
