<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 20/06/2019
 * Time: 14:41
 */

namespace Uac\SaleSys\Business;

use Uac\SaleSys\Exceptions\ApplicationException;

final class GameSingleton
{
    /**
     * @var single $instance of GameSingleton
     */
    private static $instance;

    /**
     * GameSingleton constructor.
     */
    private function __construct()
    {
    }

    /**
     * returns the game instance
     *
     * @return mixed instance of Game Singleton
     */
    public static function getInstance()
    {
        if (empty(GameSingleton::$instance)) {
            GameSingleton::$instance = new GameSingleton();
        }

        return GameSingleton::$instance;
    }

    /**
     * return a random question from the actual season
     *
     * @param int $nif of the guy that's playing
     * @return Question (random) from the current season, that has not yet been answered by that guy
     * @throws ApplicationException if there are no questions unanswered
     */
    public function getRandomQuestionOfThisSeason(int $nif): Question
    {
        $client = Client::findByNIF($nif);
        if ($client->card==null) {
            throw new ApplicationException("You don't have a card to play this game, so you can't accumulate points!");
        }

        $thisSeason = Season::timestampToSeason(now());

        //Get all questions from the current season only
        $allQuestions = Question::where('season_id', $thisSeason->id)->get();
        $newQuestions = $allQuestions->diff($client->questionsHistory);

        if ($newQuestions->count() == 0) {
            throw new ApplicationException("You have answered all of our questions.<br>Come back later!");
        }

        return $newQuestions->random();
    }
}