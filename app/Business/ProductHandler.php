<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 26/06/2019
 * Time: 21:38
 */

namespace Uac\SaleSys\Business;


class ProductHandler
{
    /**
     * Returns all Products
     *
     * @return \Illuminate\Database\Eloquent\Collection|Product[]
     */
    public function getProducts()
    {
        return Product::all();
    }

}