<?php

namespace Uac\SaleSys\Business;


use Uac\SaleSys\Exceptions\ApplicationException;

class SaleHandler
{

    /**
     * Creates a new sale to the client with the nif given
     *
     * @param int $nif client nif to identify
     * @return Sale the new sale
     * @throws ApplicationException when there is no client with the given nif
     */
    public function newSale(int $nif): Sale
    {
        $client = Client::findByNIF($nif);
        if (!$client) {
            throw new ApplicationException("Client with NIF number $nif not found.");
        }
        return Sale::newSale($client);
    }


    /**
     * Adds an item to the sale
     *
     * @param Sale $sale the sale we are going to add a new item
     * @param int $code product code to add to the item
     * @param int $quantity product quentity to add on the item
     * @throws ApplicationException occurs when there is no product with the given code
     */
    public function addItemToSale(Sale $sale, int $code, int $quantity)
    {
        $product = Product::findByProdCode($code);
        if (!$product) {
            throw new ApplicationException("Product with code $code not found.");
        }
        $sale->newItem($code, $quantity);

    }

    /**
     * Updates and Item quantity on the sale
     *
     * @param Sale $sale the sale we are going to update
     * @param int $code product code to update
     * @param int $quantity the new product quantity
     * @throws ApplicationException occurs when there is no product with the given code
     */
    public function updateItemToSale(Sale $sale, int $code, int $quantity)
    {
        $product = Product::findByProdCode($code);
        if (!$product) {
            throw new ApplicationException("Product with code $code not found.");
        }
        $sale->updateItem($code, $quantity);

    }

    /**
     * Removes and item from the sale
     *
     * @param Sale $sale the sale from where we are going to remove the item
     * @param int $code product code to remove
     * @throws ApplicationException occurs when there is no product with the given code
     */
    public function removeItemFromSale(Sale $sale, int $code)
    {
        $product = Product::findByProdCode($code);
        if (!$product) {
            throw new ApplicationException("Product with code $code not found.");
        }
        $sale->removeItem($code);

    }

    /**
     * Adds a product to the bouquet
     *
     * @param Sale $sale the sale where we are going to update the bouquet
     * @param int $bouquet_code bouquet product code to add products
     * @param int $code product code to add
     * @param int $quantity product quantity to add
     * @throws ApplicationException occurs when there is no product with the given code or a bouquet with the given code
     */
    public function addToBouquet(Sale $sale, int $bouquet_code, int $code, int $quantity)
    {
        $product = Product::findByProdCode($code);
        $bouquet = Product::findByProdCode($bouquet_code);
        if (!$product) {
            throw new ApplicationException("Product with code $code not found.");
        }
        if (!$bouquet) {

            throw new ApplicationException("Bouquet with code $bouquet_code not found.");
        }
        if ($bouquet->edited == false) {
            $new_bouquet = $bouquet->replicate();
            $new_bouquet->save();
            $sale->addProductBouquet($new_bouquet, $code, $quantity);
            foreach ($sale->items as $item) {
                if ($item->product->id == $bouquet_code) {
                    $item->product->delete();
                    $item->product->associate($new_bouquet);
                }
            }
        } else {
            $sale->addProductBouquet($bouquet, $code, $quantity);
        }
    }

    /**
     * Updates a product quantity on bouquet
     *
     * @param Sale $sale the sale where we are going to update the bouquet
     * @param int $bouquet_code bouquet product code to update products
     * @param int $code product code to update
     * @param int $quantity product quantity to update
     * @throws ApplicationException occurs when there is no product with the given code or a bouquet with the given code
     */
    public function updateBouquet(Sale $sale, int $bouquet_code, int $code, int $quantity)
    {
        $product = Product::findByProdCode($code);
        $bouquet = Product::findByProdCode($bouquet_code);
        if (!$product) {
            throw new ApplicationException("Product with code $code not found.");
        }
        if (!$bouquet) {

            throw new ApplicationException("Bouquet with code $bouquet_code not found.");
        }
        $sale->updateProductBouquet($bouquet, $code, $quantity);


    }

    /**
     * Removes a product from bouquet
     *
     * @param Sale $sale the sale where we are going to update the bouquet
     * @param int $bouquet_code bouquet product code to remove products
     * @param int $code product code to remove
     * @param int $quantity product quantity to remove
     * @throws ApplicationException occurs when there is no product with the given code or a bouquet with the given code
     */
    public function removeFromBouquet(Sale $sale, int $bouquet_code, int $code, int $quantity)
    {
        $product = Product::findByProdCode($code);
        $bouquet = Product::findByProdCode($bouquet_code);
        if ($product) {
            if ($bouquet) {
                $sale->removeProductBouquet($bouquet, $code, $quantity);
            }
            throw new ApplicationException("Bouquet with code $bouquet_code not found.");

        }
        throw new ApplicationException("Product with code $code not found.");
    }

    /**
     * Cancels a Sale
     *
     * @param Sale $sale to be deleted
     * @throws \Exception
     */
    public function cancelSale(Sale $sale)
    {
        return $sale->deleteThis();
    }

    /**
     * Finishes and closes teh sale
     *
     * @param Sale $sale to close and get the change
     * @param float $payment given by the client
     * @param float $new_total with possible discount consideration
     * @param int $points used by the client
     * @return float change from the sale to give to the client
     * @throws ApplicationException if there is no items in the sale or the amount given is lower than the total
     */
    public function pay(Sale $sale, float $payment, float $new_total, ?int $points)
    {
        try {
            return $sale->pay($payment, $new_total, $points);
        } catch (ApplicationException $e) {
            throw  $e;
        }

    }

    /**
     * gives the sale if it is not close
     *
     * @param int $id the sale id
     * @return Sale the sale with the id given
     * @throws ApplicationException if the sale is closed
     */
    public function getSale(int $id): Sale
    {
        $sale = Sale::find($id);
        if (!$sale->isOpen) {
            throw new  ApplicationException("This sale is already closed!");
        }
        return $sale;
    }
}