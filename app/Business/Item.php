<?php


namespace Uac\SaleSys\Business;


use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * Use to define one-to-one relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}