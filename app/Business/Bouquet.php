<?php


namespace Uac\SaleSys\Business;


use Uac\SaleSys\Exceptions\ApplicationException;

/**
 * @property mixed flowers
 * @property int quantity
 * @property float|int price
 */
class Bouquet extends Product
{
    protected static $singleTableType = Bouquet::class;
    protected static $persisted = ['price', 'quantity','edited'];


    /**
     * Use to define many-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function flowers()
    {
        return $this->belongsToMany(Flower::class, 'bouquet_flower', 'bouquet_id', 'flower_id')->withPivot('quantity');
    }

    /**
     * Add a product to the bouquet and checks if the product exists and if it is from the type Flower
     *
     * @param int $code product code to be add
     * @param int $quantity quantity to add
     * @throws ApplicationException occurs when there is no product with the given code or the product its not a flower
     */
    public function addProduct(int $code, int $quantity): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");

        } else {
            if ($prod->type == "Uac\SaleSys\Business\Flower") {
                if ($this->flowers->contains($prod)) {
                    $i = $this->flowers->where('code', $prod->code)->first();
                    $i->quantity += $quantity;
                    $i->save();
                } else {
                    $this->flowers()->attach($prod->id, ['quantity' => $quantity]);
                    $this->save();
                }
                $this->calculatePrice();
            }
            else{
                throw new ApplicationException("The product with the code {$code} its not a flower.");
            }
        }
    }

    /**
     * Updates a product on the bouquet and checks if the product exists and if it is in the bouquet
     *
     * @param int $code product code to be updated
     * @param int $quantity quantity to be updated
     * @throws ApplicationException occurs when there is no product with the given code or the product its not in the bouquet
     */
    public function updateProduct(int $code, int $quantity): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");

        } elseif($quantity<=0){
            $this->removeProduct($code);
        }
        else{
            if ($this->flowers->contains($prod)) {
                $i = $this->flowers->where('code', $prod->code)->first();
                $i->pivot->quantity = $quantity;
                $i->pivot->save();
                $i->save();
                $this->calculatePrice();
            } else {
                throw new ApplicationException("The product with the code {$code} does not exist in this bouquet.");
            }
        }
    }


    /**
     * Removes a product from the bouquet and checks if the product exists and if it is in the bouquet
     *
     * @param int $code product code to be withdrawn
     * @param int $quantity the quantity to be withdrawn
     * @throws ApplicationException occurs when there is no product with the given code or when the code is of a product that does not belong to the bouquet
     */
    public function removeProduct(int $code, int $quantity): void
    {
        $prod = Product::findByProdCode($code);
        if (!$prod) {
            throw new ApplicationException("The product with the code {$code} does not exist.");
        } else {
            if ($this->flowers->contains($prod)) {
                $i = $this->flowers->where('code', $prod->code)->first();
                $i->delete();
                $this->calculatePrice();
                $this->save();
            } else {
                throw new ApplicationException("The flower {$prod->description} with the code {$prod->code} does not exist in the flower bouquet.");
            }
        }

    }

    /**
     * Calculates the actual price of the bouquet
     *
     * @return float total of the sale
     */
    public function calculatePrice(): float
    {
        $this->price = 0;
        foreach ($this->flowers as $flower) {
            $this->price += $flower->pivot->quantity * 0.9 * $flower->price;
        }
        $this->save();
        return $this->price;
    }

}