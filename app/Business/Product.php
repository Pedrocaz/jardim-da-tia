<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class Product extends Model
{
    use SingleTableInheritanceTrait;

    protected $table = "products";
    protected static $singleTableTypeField = 'type';
    protected static $persisted = ['description', 'code', 'price', 'image'];
    protected static $singleTableSubclasses = [
        Flower::class,
        MaintenanceProduct::class,
        Bouquet::class
    ];

    /**
     * gives the product with the code given
     *
     * @param $code product code
     * @return Product|null Product in case it exists or null in case not
     */
    public static function findByProdCode($code): ?Product
    {
        return Product::where('code', $code)->first();
    }

    public function promotions()
    {
        return $this->belongsToMany(Promotion::class, 'product_promotion', 'product_id', 'promotion_id');
    }

    /**
     * Gives the product price and checks if that product is in a promotion
     *
     * @return float with current price of this product, taking into account its current promotion
     */
    public function getPrice(): float
    {
        $promotion = $this->promotions->filter(function ($promotion) {
            return (time() > $promotion->startDate/1000) && (time() < $promotion->endDate/1000);
        })->first();
        if ($promotion) {
            return number_format($this->price * (1 - $promotion->percentage),2);
        }
        return number_format($this->price,2);
    }
}