<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    /**
     * creats a new newsletter for the season given
     *
     * @param int $season_id season id
     * @return Newsletter the new newsletter instance
     */
    public static function createWith(int $season_id): Newsletter
    {
        $nl = new Newsletter();
        $nl->publicationDate = now();
        $nl->save();

        $sugestionsForSeason = Suggestion::findBySeason($season_id);
        $sugestionsForSeason->each(function ($suggestion) use ($nl) {
            $nl->suggestions()->attach($suggestion->id);
        });

        $promotionsForSeason = Promotion::findBySeason($season_id);
        $promotionsForSeason->each(function ($promotion) use ($nl) {
            $nl->promotions()->attach($promotion->id);
        });
        $nl->save();

        return $nl;
    }

    /**
     * Use to define many-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function suggestions()
    {
        return $this->belongsToMany(Suggestion::class);
    }

    /**
     *Use to define many-to-many relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function promotions()
    {
        return $this->belongsToMany(Promotion::class);
    }
}
