<?php

namespace Uac\SaleSys\Business;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    //
    /**
     * return instance of Winter
     *
     * @return Season
     */
    public static function getWinter(): Season
    {
        return Season::where('name', 'Winter')->first();
    }

    /**
     * return instance of Spring
     *
     * @return Season
     */
    public static function getSpring(): Season
    {
        return Season::where('name', 'Spring')->first();
    }

    /**
     * return instance of Summer
     *
     * @return Season
     */
    public static function getSummer(): Season
    {
        return Season::where('name', 'Summer')->first();
    }

    /**
     * return instance of Fall
     *
     * @return Season
     */
    public static function getFall(): Season
    {
        return Season::where('name', 'Fall')->first();
    }

    /**
     * Return a season depending on what season the timestamp belong to
     *
     * @param \DateTime $timestamp
     * @return Season
     */
    public static function timestampToSeason(\DateTime $timestamp): Season
    {
        $dayOfTheYear = $timestamp->format('z');
        if ($dayOfTheYear < 80 || $dayOfTheYear > 356) {
            return Season::getWinter();
        }
        if ($dayOfTheYear < 173) {
            return Season::getSpring();
        }
        if ($dayOfTheYear < 266) {
            return Season::getSummer();
        }
        return Season::getFall();
    }
}
