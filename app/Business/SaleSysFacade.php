<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 28/06/2019
 * Time: 19:20
 */

namespace Uac\SaleSys\Business;


class SaleSysFacade
{
    /**
     * return newsletter handler
     *
     * @return NewsletterHandler
     */
    public function getNewsletterHandler(): NewsletterHandler
    {
        return new NewsletterHandler();
    }

    /**
     * return product handler
     *
     * @return ProductHandler
     */
    public function getProductHandler(): ProductHandler
    {
        return new ProductHandler();
    }

    /**
     * return sale handler
     *
     * @return SaleHandler
     */
    public function getSaleHandler(): SaleHandler
    {
        return new SaleHandler();
    }

    /**
     * return client handler
     *
     * @return ClientHandler
     */
    public function getClientHandler(): ClientHandler
    {
        return new ClientHandler();
    }

    /**
     * return config handler
     *
     * @return ConfigHandler
     */
    public function getConfigHandler(): ConfigHandler
    {
        return new ConfigHandler();
    }

    /**
     * return game handler
     *
     * @return GameHandler
     */
    public function getGameHandler(): GameHandler
    {
        return new GameHandler();
    }
}