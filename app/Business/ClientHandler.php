<?php
/**
 * Created by PhpStorm.
 * User: Pedrocaz
 * Date: 13/06/2019
 * Time: 13:25
 */

namespace Uac\SaleSys\Business;


use Uac\SaleSys\Exceptions\ApplicationException;

class ClientHandler
{
    /**
     * returns all clients
     *
     * @return iterable
     */
    public function getClients(): iterable
    {
        return Client::all();
    }

    /**
     * Calls function addClient from Client to create a new client with the parameters given
     *
     * @param string $name client name
     * @param string $email client email
     * @param int $nif client nif
     * @param bool|null $withCard if the client wants card or not, true for positive false for negative
     * @return Client client instance after created
     * @throws ApplicationException it occurs when teh nif given does not have 9 digits or the client nif is already registered
     */
    public function addClient(string $name, string $email, int $nif, ?bool $withCard): Client
    {
        try {
            return Client::createWith($name, $email, $nif, $withCard);
        } catch (ApplicationException $e) {
            throw  $e;
        }
    }

    /**
     * Calls the function findByNIF from Client to get the client with the nif given
     *
     * @param int $nif client nif
     * @return Client|null returns the client object or null in case it doesnt exists
     */
    public function getClient(int $nif): ?Client
    {
        return Client::findByNIF($nif);
    }

    /**
     * Calls the function deleteClient from Client to delete the client object with the nif given
     *
     * @param int $nif client nif
     */
    public function deleteClient(int $nif): void
    {
        Client::deleteByNIF($nif);
    }
}