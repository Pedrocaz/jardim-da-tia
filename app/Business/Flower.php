<?php


namespace Uac\SaleSys\Business;


class Flower extends Product
{
    protected static $singleTableType = Flower::class;
    protected static $persisted = [];
}