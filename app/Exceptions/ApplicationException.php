<?php
/**
 * User: fmartins
 * Date: 07/04/18
 * Time: 20:27
 */

namespace Uac\SaleSys\Exceptions;

use Exception;

class ApplicationException extends Exception
{
}