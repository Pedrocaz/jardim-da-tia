<?php

namespace Uac\SaleSys\Http\Controllers;

use Illuminate\Http\Request;
use Uac\SaleSys\Business\Newsletter;
use Uac\SaleSys\Business\NewsletterHandler;
use Uac\SaleSys\Business\SaleSysFacade;
use Uac\SaleSys\Business\Season;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Newsletter::latest()->paginate(4);
        return view('newsletter.index', compact('news'))
            ->with('i', (request()->input('page', 1) - 1) * 4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seasons = Season::all();
        return view('newsletter.create', compact('seasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, SaleSysFacade $saleSysFacade)
    {
        $newsletterHandler = $saleSysFacade->getNewsletterHandler();

        $newsletterHandler->createNewsletterForSeason($request['season']);
        return redirect()->route('newsletter.index');
    }
}
