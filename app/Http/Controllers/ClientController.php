<?php

namespace Uac\SaleSys\Http\Controllers;

use Uac\SaleSys\Business\Client;
use Illuminate\Http\Request;
use Uac\SaleSys\Business\SaleSysFacade;
use Uac\SaleSys\Exceptions\ApplicationException;

class ClientController extends Controller
{

    const NAME = 'name';
    const EMAIL = 'email';
    const NIF = 'nif';
    const WITH_CARD = 'withCard';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->paginate(5);
        return view('client.index', compact('clients'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, SaleSysFacade $saleSysFacade)
    {
        $clientHandler = $saleSysFacade->getClientHandler();

        $request->validate([
            self::NAME => 'required',
            self::EMAIL => 'required',
            self::NIF => 'required|numeric',
            self::WITH_CARD => 'required'
        ]);

        try {
            $card = $request[self::WITH_CARD] == "true" ? true : false;
            $clientHandler->addClient(
                $request[self::NAME],
                $request[self::EMAIL],
                $request[self::NIF],
                $card
            );
            return redirect()->route('client.index')
                ->with('success', 'New client created successfully');
        } catch (ApplicationException $e) {
            return redirect()->route('client.create')
                ->withInput()
                ->withErrors(['nif' => "Error adding client: " . $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $nif
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $nif, SaleSysFacade $saleSysFacade)
    {
        $clientHandler = $saleSysFacade->getClientHandler();

        $client = $clientHandler->getClient($nif);
        $sales=$client->sales->sortByDesc('updated_at')->take(10);
        return view('client.detail', compact('client','sales'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $nif
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $nif, SaleSysFacade $saleSysFacade)
    {
        $clientHandler = $saleSysFacade->getClientHandler();

        $clientHandler->deleteClient($nif);
        return redirect()->route('client.index')
            ->with('success', 'Client deleted successfully');
    }
}
