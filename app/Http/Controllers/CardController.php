<?php

namespace Uac\SaleSys\Http\Controllers;

use Uac\SaleSys\Business\Card;
use Illuminate\Http\Request;
use Uac\SaleSys\Business\ConfigHandler;
use Uac\SaleSys\Business\ConfigHelper;
use Uac\SaleSys\Business\SaleSysFacade;

class CardController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(SaleSysFacade $saleSysFacade)
    {
        $configHandler = $saleSysFacade->getConfigHandler();

        $options = $configHandler->getOptions();
        return view('card.edit', compact('options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, SaleSysFacade $saleSysFacade)
    {
        $configHandler = $saleSysFacade->getConfigHandler();

        $money = $request['money'];
        $points = $request['points'];
        $ratio = $request['ratio'];
        $configHandler->setOptions($money, $points, $ratio);
        $options = $configHandler->getOptions();
        return redirect()->route('config.edit', 1)->with(compact('options'));
    }
}
