<?php

namespace Uac\SaleSys\Http\Controllers;


use Uac\SaleSys\Business\GameHandler;
use Uac\SaleSys\Business\ClientHandler;
use Illuminate\Http\Request;
use Uac\SaleSys\Business\SaleSysFacade;
use Uac\SaleSys\Exceptions\ApplicationException;

class QuestionController extends Controller
{

    const NIF = 'nif';
    const ANSWER_ID = 'answer';
    const QUESTION_ID = 'question_id';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('game.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, SaleSysFacade $saleSysFacade)
    {
        $gameHandler = $saleSysFacade->getGameHandler();
        $clientHandler = $saleSysFacade->getClientHandler();

        $request->validate([
            self::NIF => 'required|numeric',
        ]);
        $client = $clientHandler->getClient($request[self::NIF]);

        if (!$client) {
            return redirect()->route('game.index')
                ->withInput()
                ->withErrors(['nif' => "This client doesn't exist."]);
        }

        if ($gameHandler->clientPlayed($request[self::NIF])) {
            return redirect()->route('game.index')
                ->withInput()
                ->withErrors(['nif' => "This client already played this week."]);
        }

        return redirect()->route('game.show', $request[self::NIF]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $nif
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(int $nif, SaleSysFacade $saleSysFacade)
    {
        $gameHandler = $saleSysFacade->getGameHandler();

        try {
            $question = $gameHandler->getRandomQuestion($nif);
        } catch (ApplicationException $e) {
            return redirect()->route('game.index')
                ->withInput()
                ->withErrors(['nif' => $e->getMessage()]);

        }
        return view('game.show', compact('nif', 'question'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param int $nif
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $nif, Request $request, SaleSysFacade $saleSysFacade)
    {
        $gameHandler = $saleSysFacade->getGameHandler();

        $request->validate([
            self::ANSWER_ID => 'required|numeric',
            self::QUESTION_ID => 'required|numeric'
        ]);

        if ($gameHandler->checkAnswer($nif, $request[self::QUESTION_ID], $request[self::ANSWER_ID])) {
            //Right Answer
            return redirect()->route('game.index')
                ->with('type', 'success')
                ->with('message', 'That\'s correct!<br>1 Point awarded.<br>Come back next week for more questions.');
        } else {
            //Wrong Answer
            return redirect()->route('game.index')
                ->with('type', 'danger')
                ->with('message', 'You got it wrong!<br>Come back next week for more questions.');
        }
    }

    /**
     * Reset the playing status of all clients
     *
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(SaleSysFacade $saleSysFacade)
    {
        $gameHandler = $saleSysFacade->getGameHandler();

        $gameHandler->resetPlayers();
        return redirect()->route('game.index');
    }

}
