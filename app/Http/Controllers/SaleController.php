<?php

namespace Uac\SaleSys\Http\Controllers;

use Illuminate\Http\Request;
use Uac\SaleSys\Business\SaleSysFacade;
use Uac\SaleSys\Exceptions\ApplicationException;

class SaleController extends Controller
{
    const NIF = 'nif';
    const CODE = 'code';
    const QUANTITY = 'quantity';
    const QUANTITY_UPDATE = 'quantityUpdate';
    const PAYMENT = 'payment';

    /**
     * Show the form for creating a new sale.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SaleSysFacade $saleSysFacade)
    {
        $productHandler = $saleSysFacade->getProductHandler();

        $products = $productHandler->getProducts();
        return view('sale.create', compact('products'));
    }

    /**
     * @param Request $request
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, SaleSysFacade $saleSysFacade)
    {
        $saleHandler = $saleSysFacade->getSaleHandler();

        $request->validate([
            self::NIF => 'required',
        ]);

        try {
            $sale = $saleHandler->newSale(\request(self::NIF));
            return redirect()->route('sale.edit', $sale->id);
        } catch (ApplicationException $e) {
            return redirect()->route('sale.create')
                ->withInput()
                ->withErrors(['nif' => "Error initiating new sale: " . $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id, SaleSysFacade $saleSysFacade)
    {
        $productHandler = $saleSysFacade->getProductHandler();
        $products = $productHandler->getProducts();

        $configHandler = $saleSysFacade->getConfigHandler();
        $options = $configHandler->getOptions();

        $saleHandler = $saleSysFacade->getSaleHandler();
        try {
            $sale = $saleHandler->getSale($id);
            return view('sale.edit', compact('products', 'sale', 'options'));
        } catch (ApplicationException $e) {
            return redirect()->route('sale.create')
                ->withErrors(['code' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id, SaleSysFacade $saleSysFacade)
    {
        $request->validate([
            self::CODE => 'required',
            self::QUANTITY => 'required'
        ]);

        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($id);
            $saleHandler->addItemToSale($sale, $request[self::CODE], $request[self::QUANTITY]);
            return redirect()->route('sale.edit', $id)
                ->with('success', 'Item added successfully');
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $id)
                ->withInput()
                ->withErrors(['code' => "Error adding item: " . $e->getMessage()]);
        }

    }

    /**
     * Removes an item from the sale
     *
     * @param int $sale_id
     * @param int $code
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeItem(int $sale_id, int $code, SaleSysFacade $saleSysFacade)
    {
        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($sale_id);
            $saleHandler->removeItemFromSale($sale, $code);
            return redirect()->route('sale.edit', $sale_id);
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $sale_id)
                ->withInput()
                ->withErrors(['code' => "Error removing item: " . $e->getMessage()]);
        }
    }

    /**
     * Updates an item from the sale
     *
     * @param Request $request
     * @param int $sale_id
     * @param int $code
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateItem(Request $request, int $sale_id, int $code, SaleSysFacade $saleSysFacade)
    {
        $request->validate([
            self::QUANTITY_UPDATE => 'required'
        ]);

        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($sale_id);
            $saleHandler->updateItemToSale($sale, $code, $request[self::QUANTITY_UPDATE]);
            return redirect()->route('sale.edit', $sale_id);
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $sale_id)
                ->withInput()
                ->withErrors(['code' => "Error updating item: " . $e->getMessage()]);
        }
    }

    /**
     * Cancel a sale
     *
     * @param int $id
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function cancel(int $id, SaleSysFacade $saleSysFacade)
    {
        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($id);
            $saleHandler->cancelSale($sale);
            return redirect()->route('sale.create');
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $id)
                ->withInput()
                ->withErrors(['code' => "Error canceling sale: " . $e->getMessage()]);
        }
    }

    /**
     * Closes and finishes the sale
     *
     * @param Request $request
     * @param int $id
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function pay(Request $request, int $id, SaleSysFacade $saleSysFacade)
    {
        $request->validate([
            self::PAYMENT => 'required|numeric'
        ]);
        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($id);
            $newTotal = $request['total'];
            $pointsUsed = $request['points'];
            $change = $saleHandler->pay($sale, $request[self::PAYMENT], $newTotal, $pointsUsed);

            $payment = $request[self::PAYMENT];
            return view('sale.receipt', compact('sale', 'change', 'newTotal', 'pointsUsed', 'payment'));

        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $id)
                ->withInput()
                ->withErrors(['code' => "Error paying sale: " . $e->getMessage()]);
        }
    }


    /**
     * update the bouquet product
     *
     * @param Request $request http request
     * @param int $sale_id
     * @param int $code
     * @param int $extra
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateExtra(Request $request, int $sale_id, int $code, SaleSysFacade $saleSysFacade)
    {
        $request->validate([
            self::QUANTITY_UPDATE => 'required'
        ]);

        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($sale_id);
            $saleHandler->updateItemToSale($sale, $code, $request[self::QUANTITY_UPDATE]);
            return redirect()->route('sale.edit', $sale_id);
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $sale_id)
                ->withInput()
                ->withErrors(['code' => "Error updating item: " . $e->getMessage()]);
        }
    }


    /**
     * Remove flower from bouquet
     *
     * @param int $sale_id
     * @param int $code product code to remove
     * @param int $extra
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeExtra(int $sale_id, int $code, int $extra, SaleSysFacade $saleSysFacade)
    {
        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($sale_id);
            $saleHandler->removeFromBouquet($sale, $code, $extra);
            return redirect()->route('sale.edit', $sale_id);
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $sale_id)
                ->withInput()
                ->withErrors(['code' => "Error removing extra flower from Bouquet: " . $e->getMessage()]);
        }
    }

    /**
     * Add extra flower to bouquet
     *
     * @param Request $request http request
     * @param int $sale_id sale id where the bouquet is
     * @param int $code product code to add
     * @param SaleSysFacade $saleSysFacade
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addExtra(Request $request, int $sale_id, int $code,  SaleSysFacade $saleSysFacade)
    {
        $request->validate([
            self::CODE => 'required',
            self::QUANTITY => 'required'
        ]);

        try {
            $saleHandler = $saleSysFacade->getSaleHandler();
            $sale = $saleHandler->getSale($sale_id);

            $saleHandler->addToBouquet($sale, $code, $request[self::CODE], $request[self::QUANTITY]);
            return redirect()->route('sale.edit', $sale_id)
                ->with('success', 'Extra flower added to bouquet successfully');
        } catch (ApplicationException $e) {
            return redirect()->route('sale.edit', $sale_id)
                ->withInput()
                ->withErrors(['code' => "Error adding extra flower to bouquet: " . $e->getMessage()]);
        }
    }
}
