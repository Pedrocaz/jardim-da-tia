<?php $__env->startSection('content'); ?>

    <h1>List Customers</h1>

    <table>
        <tr>
            <th>Designation</th>
            <th>VAT number</th>
            <th>Phone number</th>
            <th>Discount type</th>
        </tr>
        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($customer->designation); ?></td>
                <td><?php echo e($customer->vatNumber); ?></td>
                <td><?php echo e($customer->phoneNumber); ?></td>
                <td><?php echo e($customer->discount->description); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>