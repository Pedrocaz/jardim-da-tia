<?php $__env->startSection('content'); ?>

    <h1>Novo Aluno</h1>

    <?php if($errors->any()): ?>
        <div>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <form action="/aluno/new" method="post">

        <?php echo e(csrf_field()); ?>


        <div>
            <label for="numero">Numero:</label>
            <input id="numero" type="number" name="numero" size="50" value="<?php echo e(old('numero')); ?>">
        </div>

        <div>
            <label for="nome">Nome:</label>
            <input id="nome" type="text" name="nome" value="<?php echo e(old('nome')); ?>"/>
        </div>

        <div>
            <input type="submit" value="Adicionar Aluno">
        </div>

    </form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>