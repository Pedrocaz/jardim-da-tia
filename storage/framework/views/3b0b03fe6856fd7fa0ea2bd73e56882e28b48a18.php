<?php $__env->startSection('messages'); ?>
    <?php if($message=Session::get('success')): ?>
        <div class="alert alert-success">
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Client List</h3>
            </div>
            <div class="col-sm-2">
                <a class="btn btn-sm btn-success" href="<?php echo e(route('client.create')); ?>">Create new Client</a>
            </div>
        </div>

        <table class="table table-hover table-sm">
            <thead class="table-">
            <tr>
                <th>Nº</th>
                <th>Name</th>
                <th>Email</th>
                <th>NIF</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e(++$i); ?></td>
                    <td><?php echo e($client->name); ?></td>
                    <td><?php echo e($client->email); ?></td>
                    <td><?php echo e($client->nif); ?></td>
                    <td>
                        <form action="<?php echo e(route('client.destroy',$client->nif)); ?>" method="post">
                            <a class="btn btn-sm btn-info" href="<?php echo e(route('client.show',$client->nif)); ?>">Details</a>
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('DELETE'); ?>
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <?php echo $clients->links(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>