<?php $__env->startSection('messages'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <form action="<?php echo e(route('config.update',1)); ?>" method="post">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="card col-5 mx-auto border-2 border-success p-0">
                <div class="card-header bg-success text-white">
                    <h3>Card Config</h3>
                </div>
                <div class="card-body">
                    <div class="mb-4 form-inline justify-content-center">
                        <label for="money">Money Threshold:&nbsp;</label>
                        <input type="number" name="money" class="form-control col-3" value="<?php echo e($options[0]); ?>" required/>
                        <label for="money">&nbsp;€</label>
                    </div>
                    <hr class="bg-success">
                    <div class="mb-4 form-inline justify-content-center">
                        <label for="points">Points received when threshold is met:&nbsp;</label>
                        <input type="number" name="points" class="form-control col-3" value="<?php echo e($options[1]); ?>" required/>
                    </div>
                    <hr class="bg-success">
                    <div class="form-inline justify-content-center">
                        <label for="ratio">Ratio of&nbsp;</label>
                        <input type="number" name="ratio" class="form-control col-3" value="<?php echo e($options[2]); ?>" required/>
                        <label for="ratio">&nbsp;points per 1€</label>
                    </div>
                </div>

                <div class="card-footer bg-success text-center">
                    <button type="submit" class="btn btn-dark">Update</button>
                </div>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>