<?php $__env->startSection('content'); ?>

    <div class="mx-auto jumbotron col-md-10 m-0 pb-3">
        <div class="row justify-content-center">
            <h3><?php echo e($question->question); ?></h3>
        </div>
        <form action="<?php echo e(route('game.update',$nif)); ?>" method="post">
            <?php echo method_field('PUT'); ?>
            <?php echo csrf_field(); ?>

            <div class="form-row justify-content-around mt-3 p-0">
                <div class="btn-group btn-group-toggle col-lg-10 m-1 p-0" data-toggle="buttons">
                    <?php $__currentLoopData = $question->answers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $answer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <label class="btn btn-outline-primary btn-lg form-check-label col-lg-10 m-1 ">
                            <input class="form-check-input" type="radio" name="answer"
                                   value="<?php echo e($answer->id); ?>"/><?php echo e($answer->answer); ?>

                        </label>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <input type="hidden" name="question_id" value="<?php echo e($question->id); ?>">
            <div class="col-lg-10 p-0 mx-auto">
                <div class="form-row my-3 p-0  justify-content-center">
                    <input type="submit" class="btn btn-success" value="Answer"/>
                </div>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>