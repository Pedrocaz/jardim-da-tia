<?php $__env->startSection('messages'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="jumbotron col-5 mx-auto border border-success py-4">
        <h3 class="text-center">Generate Newsletter</h3>
        <hr>
        <form class="mb-0" action="<?php echo e(route('newsletter.store')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="form-inline justify-content-around">
                <label for="season">Generate the newsletter for the&nbsp;<strong>season</strong>:</label>
                <select id="season" class="custom-select" name="season">
                    <?php $__currentLoopData = $seasons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $season): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($season->id); ?>"><?php echo e($season->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="row justify-content-between">
                <a href="<?php echo e(route('newsletter.index')); ?>" class="btn btn-sm btn-success m-3">Back</a>
                <button type="submit" class="btn btn-sm btn-primary m-3">Generate</button>
            </div>

        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>