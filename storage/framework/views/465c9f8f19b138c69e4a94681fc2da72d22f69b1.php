<?php $__env->startSection('messages'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row justify-content-center">
            <form action="<?php echo e(route('sale.store')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-success text-white">NIF</span>
                    </div>
                    <input type="number" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="nif"
                           required>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit" id="button-addon2">New Sale</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>