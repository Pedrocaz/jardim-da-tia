<table class="table table-striped table-hover table-sm tab border-success">
    <thead class="bg-success text-white">
    <tr>
        <th colspan="5" class="text-center">Products</th>
    </tr>
    <tr>
        <th>Image</th>
        <th>Code</th>
        <th>Description</th>
        <th>Category</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td class="align-middle"><img src="<?php echo $product->image; ?>" height="50px"/></td>
            <td class="align-middle"><?php echo e($product->code); ?></td>
            <td class="align-middle"><?php echo e($product->description); ?></td>
            <td class="align-middle"><?php echo Str::after($product->type,'Uac\SaleSys\Business\\'); ?></td>
            <td class="align-middle"><span class="text-<?php echo e($product->price > $product->getPrice() ? "danger" : "dark"); ?>"><?php echo e($product->getPrice()); ?>€</span></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>

