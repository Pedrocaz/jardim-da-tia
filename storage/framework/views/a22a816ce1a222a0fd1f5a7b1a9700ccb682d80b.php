<?php $__env->startSection('messages'); ?>
    <?php if($message=Session::get('success')): ?>
        <div class="alert alert-success">
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row">
            <div class="col-8">
                <form action="<?php echo e(route('sale.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">NIF</span>
                        </div>
                        <input type="number" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="nif"
                               required>
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit" id="button-addon2">New Sale</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4 m-0 p-0">
                <?php echo $__env->make('product.index', ['products' => $products], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>