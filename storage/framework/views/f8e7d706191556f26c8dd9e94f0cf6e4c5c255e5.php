<?php $__env->startSection('messages'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if($type=Session::get('type')): ?>
        <div class="alert alert-<?php echo e($type); ?>">
            <p><?php echo Session::get('message'); ?></p>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-2">
                <a class="btn btn-sm btn-success" href="<?php echo e(route('newsletter.create')); ?>">Generate New Newsletter</a>
            </div>
        </div>
        <div class="row justify-content-around">
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card text-center p-0 mt-5 mx-auto col-5 border-success">
                    <div class="card-header bg-success text-white">
                        <h5 class="card-title">Newsletter #<?php echo e($new->id); ?> - <?php echo e($new->suggestions[0]->season->name); ?></h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <?php $__currentLoopData = $new->suggestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $suggestion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="list-group-item border-success"><?php echo e($suggestion->text); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                        <hr class="bg-success">

                        <?php if(!$new->promotions->isEmpty()): ?>
                            <div class="bd-example">
                                <div id="newsletter<?php echo e($loop->iteration); ?>" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <?php $__currentLoopData = $new->promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li data-target="#newsletter" data-slide-to="<?php echo e($loop->iteration); ?>"
                                                class="bg-success <?php if($loop->first): ?> active <?php endif; ?>"></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <?php $__currentLoopData = $new->promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="carousel-item <?php if($loop->first): ?> active <?php endif; ?>">
                                                <img src="<?php echo e($promotion->products[0]->image); ?>"
                                                     class="d-block w-100">
                                                <div class="carousel-caption rounded text-danger"
                                                     style="background-color: rgba(128,128,128,0.25)">
                                                    <p class="h5"><?php echo e($promotion->description); ?></p>
                                                    <p class="h6"><?php echo e($promotion->products[0]->description); ?>

                                                        with <strong><?php echo e($promotion->percentage*100); ?>%</strong> Discount
                                                    </p>
                                                    <p class="h6">Before: <?php echo e($promotion->products[0]->price); ?>€
                                                        <strong>Now: <?php echo e(number_format($promotion->products[0]->price*(1-$promotion->percentage),2)); ?>

                                                            €</strong>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <a class="carousel-control-prev text-success"
                                       href="#newsletter<?php echo e($loop->iteration); ?>"
                                       role="button"
                                       data-slide="prev">
                                        <span><i class="fa fa-2x fa-angle-left " aria-hidden="true"></i></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next text-success"
                                       href="#newsletter<?php echo e($loop->iteration); ?>"
                                       role="button"
                                       data-slide="next">
                                        <span><i class="fa fa-2x fa-angle-right " aria-hidden="true"></i></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        <?php else: ?>
                            <ul class="list-group">
                                <li class="list-group-item border-success">There are currently no promotions at this
                                    season.
                                </li>
                            </ul>
                        <?php endif; ?>
                    </div>

                    <div class="card-footer bg-success text-white">
                        Created at <?php echo e(date('M d, o', strtotime($new->publicationDate))); ?>

                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>