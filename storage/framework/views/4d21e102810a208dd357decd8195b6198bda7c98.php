<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="card text-center p-0 mt-5 mx-auto col-6 border-success">
                <div class="card-header bg-success text-white">
                    <h5 class="card-title">Jardim da Tia</h5>
                </div>
                <div class="card-body p-0">
                    <table class="table table-sm border-success col-12 m-0">
                        <thead class="bg-success text-white">
                        <tr>
                            <th class="text-center" colspan="7"><?php echo e($sale->client->name); ?>'s Sale Items</th>
                        </tr>
                        <tr>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Sub-total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $sale->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="align-middle"><?php echo e($item->product->code); ?></td>
                                <td class="align-middle"><?php echo e($item->product->description); ?></td>
                                <td class="align-middle"><?php echo e($item->product->price); ?>€</td>
                                <td class="align-middle"><?php echo e($item->quantity); ?></td>
                                <td class="align-middle"><?php echo e($item->quantity*$item->product->getPrice()); ?>€</td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <hr class="bg-success">
                    <?php if($pointsUsed): ?>
                        <p class="h5">You used <?php echo e($pointsUsed); ?> points.</p>
                    <?php endif; ?>
                    <p class="h5">Total: <?php echo e($newTotal); ?>€</p>
                    <p class="h5">Paid: <?php echo e($payment); ?>€</p>
                    <p class="h5">Change: <?php echo e($change); ?>€</p>
                </div>
                <div class="card-footer bg-success text-white">
                    Processed at <?php echo e(date('Y-m-d H:i:s')); ?>

                </div>
            </div>

        </div>
        <div class="row justify-content-center">
            <a href="<?php echo e(route('sale.create')); ?>" class="btn btn-sm btn-success m-3">Continue</a>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>