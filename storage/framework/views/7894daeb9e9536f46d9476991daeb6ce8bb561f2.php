<?php $__env->startSection('content'); ?>

    <h1>Lista de Alunos</h1>

    <table>
        <tr>
            <th>Número</th>
            <th>Nome</th>
        </tr>
        <?php $__currentLoopData = $alunos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aluno): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($aluno->numero); ?></td>
                <td><?php echo e($aluno->nome); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>