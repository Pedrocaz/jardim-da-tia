<?php $__env->startSection('messages'); ?>
    <?php if($message=Session::get('success')): ?>
        <div class="alert alert-success">
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><?php echo e($client->name); ?>'s Details</h3>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Name:</strong> <?php echo e($client->name); ?>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Email:</strong> <?php echo e($client->email); ?>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>NIF:</strong> <?php echo e($client->nif); ?>

                </div>
            </div>
            <table class="table table-bordered table-sm col-sm-1">
                <thead class="table-">
                <tr>
                    <th colspan="2">Card</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Points</td>
                        <td><?php echo e($client->card->points); ?></td>
                    <tr></tr>
                </tbody>
            </table>
            <div class="col-md-12">
                <a href="<?php echo e(route('client.index')); ?>" class="btn btn-sm btn-success">Back</a>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>