<?php $__env->startSection('messages'); ?>
    <?php if($message=Session::get('success')): ?>
        <div class="alert alert-success">
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="row col-xl-8">
                <!--ADD ITEM SECTION-->
                <div class="row col-12 justify-content-center">
                    <form class="w-100" action="<?php echo e(route('sale.update',$sale->id)); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('PUT'); ?>
                        <div class="input-group mb-3 w-100">
                            <div class="input-group-prepend"><span
                                        class="input-group-text bg-success text-white">Code</span></div>
                            <input type="number" class="form-control" name="code" required>

                            <div class="input-group-prepend"><span class="input-group-text  bg-success text-white ">Quantity</span>
                            </div>
                            <input type="number" class="form-control col-2" name="quantity" value="1" required>

                            <div class="input-group-append">
                                <button class="btn btn-outline-success " type="submit" id="button-addon2">Add item
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--ADD ITEM SECTION END-->
                <!--LIST ITEM SECTION-->
                <div class="row col-12 justify-content-center">
                    <?php if($sale->items->count()>0): ?>
                        <table class="table table-striped table-hover table-sm tab border-success col-12">
                            <thead class="bg-success text-white">
                            <tr>
                                <th class="text-center" colspan="7"><?php echo e($sale->client->name); ?>'s Sale Items</th>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Sub-total</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $sale->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="align-middle"><img src="<?php echo $item->product->image; ?>" height="50px"/></td>
                                    <td class="align-middle"><?php echo e($item->product->code); ?></td>
                                    <td class="align-middle"><?php echo e($item->product->description); ?></td>
                                    <td class="align-middle"><span
                                                class="text-<?php echo e($item->product->price > $item->product->getPrice() ? "danger" : "dark"); ?>"><?php echo e($item->product->getPrice()); ?>€</span>
                                    </td>
                                    <td class="align-middle">
                                        <input form="updateForm" type="number" name="quantityUpdate"
                                               value="<?php echo e($item->quantity); ?>"/>
                                    </td>
                                    <td class="align-middle"><?php echo e($item->quantity*$item->product->getPrice()); ?>€</td>
                                    <td class="d-flex align-self-center justify-content-around">
                                        <form class="m-0" id="updateForm" action="<?php echo e($item->product->code); ?>/update"
                                              method="post">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                            <button type="submit" class="btn btn-sm btn-info">
                                                <i class="fas fa-sync"></i>
                                            </button>
                                        </form>
                                        <form class="m-0" action="<?php echo e($item->product->code); ?>/remove" method="post">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i class="fas fa-times"></i>
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                                <!-- BOUQUET START-->
                                <?php if($item->product->type=='Uac\SaleSys\Business\Bouquet'): ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="6">
                                            <table class="table table-sm table-hover m-0 text-center">
                                                <thead>
                                                <tr>
                                                    <th>Code</th>
                                                    <th>Description</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php $__currentLoopData = $item->product->flowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($flower->code); ?></td>
                                                        <td><?php echo e($flower->description); ?></td>
                                                        <td><?php echo e($flower->price); ?></td>
                                                        <td>
                                                            <input form="updateBouquet" type="number"
                                                                   name="quantityExtra"
                                                                   value="<?php echo e($flower->pivot->quantity); ?>" disabled/>
                                                        </td>
                                                        <td class="d-flex align-self-center justify-content-around">
                                                            <form class="m-0" id="updateBouquet"
                                                                  action="<?php echo e($item->product->code); ?>/<?php echo e($flower->code); ?>/updateExtra"
                                                                  method="post">
                                                                <?php echo csrf_field(); ?>
                                                                <?php echo method_field('PUT'); ?>
                                                                <button type="submit" class="btn btn-sm btn-info" disabled>
                                                                    <i class="fas fa-sync"></i>
                                                                </button>
                                                            </form>
                                                            <form class="m-0"
                                                                  action="<?php echo e($item->product->code); ?>/<?php echo e($flower->code); ?>/removeExtra"
                                                                  method="post">
                                                                <?php echo csrf_field(); ?>
                                                                <?php echo method_field('PUT'); ?>
                                                                <button type="submit" class="btn btn-sm btn-danger" disabled>
                                                                    <i class="fas fa-times"></i>
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </tbody>
                                            </table>
                                            <form class="w-100 m-0" action="<?php echo e($item->product->code); ?>/addExtra" method="post">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('PUT'); ?>
                                                <div class="input-group m-0 w-100 ">
                                                    <div class="input-group-prepend"><span
                                                                class="input-group-text bg-success text-white">Code</span></div>
                                                    <input type="number" class="form-control" name="code" required disabled>

                                                    <div class="input-group-prepend"><span class="input-group-text  bg-success text-white ">Quantity</span>
                                                    </div>
                                                    <input type="number" class="form-control col-2" name="quantity" value="1" required disabled>

                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-success " type="submit" id="button-addon2" disabled>Add to Bouquet
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <!-- BOUQUET END-->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
                <!--LIST ITEM SECTION END-->
                <!--SALE ACTIONS SECTION-->
                <div class="row col-12 justify-content-between">
                    <form action="cancel" method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('DELETE'); ?>
                        <button class="btn btn-danger" type="submit">
                            <i class="fas fa-trash"></i>
                            Cancel Sale
                        </button>

                    </form>
                    <form action="" method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('PUT'); ?>
                        <div class="input-group mb-3 w-100">
                            <div class="input-group-prepend"><span
                                        class="input-group-text  bg-success text-white">Total</span>
                            </div>
                            <div class="input-group-prepend"><span
                                        class="input-group-text bg-white text-success"><?php echo e(number_format($sale->calculateTotal(),2)); ?></span>
                            </div>
                            <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                    data-target="#paymnent">
                                Pay
                            </button>
                        </div>
                    </form>
                </div>
                <!--SALE ACTIONS SECTION END-->
            </div>
            <div class="col-xl-4 m-0 p-0">
                <!--LIST PRODUCT SECTION-->
            <?php echo $__env->make('product.index', ['products' => $products], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!--LIST PRODUCT SECTION END-->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<!-- Modal Payment Section-->
<div class="modal fade" id="paymnent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="payment" action="pay" method="post">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    <div class="form-inline row">
                        <label for="total" class="col-form-label col-6 justify-content-end">Total:</label>
                        <input type="number" class="form-control bg-white text-success  col-2" id="total"
                               name="total" step="any" value="<?php echo e($sale->calculateTotal()); ?>" readonly/>
                        <label for="total" class="col-form-label col-4 justify-content-start pl-1">€</label>
                    </div>
                    <div class="form-inline row">
                        <label for="payment" class="col-form-label col-6 justify-content-end">Insert Money:</label>
                        <input class="form-control col-2" id="payment" name="payment" type="number" step="any">
                        <label for="payment" class="col-form-label col-4 justify-content-start pl-1">€</label>
                    </div>
                    <?php if($sale->client->card): ?>
                        <hr class="bg-success">
                        <div class="form-inline row align-items-center justify-content-center">
                            <label for="points" class="col-form-label justify-content-end">Card Points:</label>
                            <input class="form-control col-2 m-1" type="number" id="points" name="points" readonly/>
                            <input type="range" class="custom-range col-6 p-3" min="0"
                                   max="<?php echo e($sale->client->card->points); ?>"
                                   value="0" id="slider" name="slider">
                        </div>
                    <?php endif; ?>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Back</button>
                <button form="payment" type="submit" class="btn btn-success">Pay</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Payment Section END-->
<script>
    let slider = document.getElementById("slider");
    let output = document.getElementById("points");
    let total = document.getElementById("total");
    output.value = slider.value; // Display the default slider value
    // Update the current slider value (each time you drag the slider handle)
    slider.oninput = function () {
        output.value = this.value;
        total.value = (<?php echo e($sale->calculateTotal()); ?> -this.value / <?php echo e($options[2]); ?>).toFixed(2);//options[2] is ratio of points to euro
    }
</script>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>