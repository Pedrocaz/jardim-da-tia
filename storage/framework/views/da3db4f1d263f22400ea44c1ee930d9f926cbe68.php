<?php $__env->startSection('messages'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <strong>Error! </strong> there where some problems with your input.<br>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if($type=Session::get('type')): ?>
        <div class="alert alert-<?php echo e($type); ?>">
            <p><?php echo Session::get('message'); ?></p>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <form action="<?php echo e(route('game.store')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon3">NIF</span>
                    </div>
                    <input type="number" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="nif"
                           required>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit" id="button-addon2">Play</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row justify-content-center mt-3">
            <form action="game/reset" method="post">
                <?php echo csrf_field(); ?>
                <input type="submit" class="btn btn-lg btn-warning" value="Reset played status"/>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>