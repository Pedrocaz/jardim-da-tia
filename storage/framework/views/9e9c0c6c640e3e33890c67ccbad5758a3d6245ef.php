<?php $__env->startSection('content'); ?>

    <h1>New Customer</h1>

    <?php if($errors->any()): ?>
        <div>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <form action="/customers/new" method="post">

        <?php echo e(csrf_field()); ?>


        <div>
            <label for="designation">Designation:</label>
            <input id="designation" type="text" name="designation" size="50" value="<?php echo e(old('designation')); ?>">
        </div>

        <div>
            <label for="vatNumber">VAT number:</label>
            <input id="vatNumber" type="number" name="vatNumber" value="<?php echo e(old('vatNumber')); ?>"/>
        </div>

        <div>
            <label for="phoneNumber">Phone number:</label>
            <input id="phoneNumber" type="text" name="phoneNumber" value="<?php echo e(old('phoneNumber')); ?>"/>
        </div>

        <div>
            <label for="discountType">Discount type:</label>
            <select id="discountType" name="discountType">
                <?php $__currentLoopData = $discounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(old('discountType') == $discount->id): ?>
                        <option selected="selected" value="<?php echo e($discount->id); ?>"><?php echo e($discount->description); ?></option>
                    <?php else: ?>
                        <option value="<?php echo e($discount->id); ?>"><?php echo e($discount->description); ?></option>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
        </div>

        <div>
            <input type="submit" value="Add customer">
        </div>

    </form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>