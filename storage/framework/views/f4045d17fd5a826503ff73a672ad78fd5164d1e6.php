<nav class="navbar sticky-top navbar-dark navbar-expand-lg bg-success">
    <div class="row col-md-10 mx-auto">
        <a class="navbar-brand text-white" href="#">
            <h1>Jardim da Tia</h1>
        </a>
        <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarToggler"
                aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 w-100 justify-content-end ">
                <li class="nav-item">
                    <a class="nav-link" href="/"><i class="fas fa-th m-1"></i>Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/newsletter"><i class="far fa-newspaper m-1"></i>Newsletter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="<?php echo e(route('sale.create')); ?>"><i class="fas fa-shopping-cart m-1"></i>Sale</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/client"><i class="fas fa-users m-1"></i>Clients</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/game"><i class="fas fa-gamepad m-1"></i>Game</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/config/1/edit"><i class="fas fa-cog m-1"></i>Config</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
