<?php $__env->startSection('content'); ?>
    <head>
        <title>Gestão de Clientes</title>
    </head>

    <body>
    <td>
        <button type="button" onclick="window.location.href='/client/new'">Adicionar Cliente</button>
    </td>

    <table>
        <thead>
        <tr>
            <td>Nome</td>
            <td>Email</td>
            <td>NIF</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($client->name); ?></td>
                <td><?php echo e($client->email); ?></td>
                <td><?php echo e($client->nif); ?></td>
                <td>
                    <button type="button" onclick="window.location.href='/client/<?php echo e($client->nif); ?>'">Detalhes</button>
                    <button type="button" onclick="window.location.href='/client/delete/<?php echo e($client->nif); ?>'">Apagar</button>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    </body>
    </html>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>