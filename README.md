# Jardim da Tia

A mix of a e-commerce with a point of sale project that sells flowers and other stuff.

## Getting Started

1. Clone this project.
    ```
    git clone https://gitlab.com/Pedrocaz/jardim-da-tia.git
    ```
2. Install dependencies.
    ```
    composer install
    ```
3. Pray.
4. Run it.
    ```
    php artisan serve
    ```