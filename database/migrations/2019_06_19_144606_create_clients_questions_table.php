<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_question', function (Blueprint $table) {
        $table->integer('client_id');
        $table->integer('question_id');
        $table->foreign('client_id')->references('id')->on('clients');
        $table->foreign('question_id')->references('id')->on('questions');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_question');
    }
}
