<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsletterSuggestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter_suggestion', function (Blueprint $table) {
            $table->integer('newsletter_id');
            $table->integer('suggestion_id');
            $table->foreign('newsletter_id')->references('id')->on('newsletters');
            $table->foreign('suggestion_id')->references('id')->on('suggestions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletter_suggestion');
    }
}
