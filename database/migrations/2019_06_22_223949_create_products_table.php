<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->unique();
            $table->string('type');
            $table->string('description', 40);
            $table->float('price');
            $table->integer('season_id')->nullable();
            $table->foreign('season_id')->references("id")->on('seasons');
            $table->string('image');
            $table->tinyint('edited')->nullable();
            $table->timestamps();
        });

        Schema::create('bouquet_flower', function (Blueprint $table) {
            $table->integer('bouquet_id');
            $table->integer('flower_id');
            $table->foreign('bouquet_id')->references("id")->on('product');
            $table->foreign('flower_id')->references("id")->on('product');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
